import json
import requests
import multiprocessing
from concurrent.futures import ThreadPoolExecutor


def parallel_domain_check(subdomain, domain, passed_subdomains):
    try:
        url_to_check = f"http://{subdomain}."+domain

        try:
            requests.get(url_to_check, timeout=1)

        except requests.ConnectionError:
            pass

        else:
            print(subdomain)
            passed_subdomains.append(subdomain)
    except BaseException as e:
        print(e)


def check_subdomain(domain, subdomain_list):
    passed_subdomains = multiprocessing.Manager().list()

    if not len(subdomain_list):
        with open("subdomains-1000.txt") as f:
            subdomain_list = f.read()
        subdomains = subdomain_list.splitlines()
    else:
        subdomains = subdomain_list
    subdomains = subdomains[:10]
    with ThreadPoolExecutor(max_workers=8) as executor:
        for subdomain in subdomains:
            executor.submit(parallel_domain_check, subdomain,
                            domain, passed_subdomains,)
    passed_subdomains_list = []
    for subdomain in passed_subdomains:
        passed_subdomains_list.append(subdomain)
    return passed_subdomains_list

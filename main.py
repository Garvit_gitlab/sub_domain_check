from fastapi import FastAPI, Form, Request
from check_subdomain import check_subdomain
import json
from fastapi.middleware.cors import CORSMiddleware


app = FastAPI()


app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.post('/subdomain/')
async def root(request: Request):
    data = json.loads(await request.body())
    data = check_subdomain(data['domain'], data['subdomains_list'].split(','))
    return {"subdomains": data}
